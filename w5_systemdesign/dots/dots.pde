import processing.video.*;
import gab.opencv.*;



float sc,b, bright, total;

float dotDensity = .40; // higher ratios = more dots
float  dampforce = 0.7;
float  kRadiusFactor = 0.5;


OpenCV opencv;
Capture cam;
int dimension;
PImage output;

float  kSpeed = 3.0;
float scale = 0.5;
float  minDistFactor = 2.5; // area of influence - smaller numbers make rendering go faster
int  dotamount = 3000;

int dancerslowness = 5;


PImage currentpic; // for dithering initialilzation...
int picamount = 19;

PImage[] dancing = new PImage[picamount];

class Dot
{
  float  x,y,vx,vy,rad,fx,fy,wt;

  Dot(float x1, float y1)
  {
    x=x1;
    y=y1;
    vx=0;
    rad=1;
    vy=0;
  }
}

Dot[] dots;

float minR, maxR, midR;

void setup()
{
  size(640, 360);
  int w = 640;
  int h = 360;
  cam = new Capture(this,int(w*scale),int(h*scale));

  opencv = new OpenCV(this, cam.width, cam.height);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  

  cam.start();

  // init to empty image
  output = new PImage(cam.width, cam.height);
  
  for (int i = 0; i < picamount; ++i) {
    dancing[i] = loadImage(i+".jpg");
  }
  currentpic = dancing[0];
  sc = width/(float)w;

  dots = new Dot[dotamount];

  for (int i=0; i<dotamount;i++) {
    dots[i]=new Dot(random(width), random(height));
  }

  frameRate(24);
  smooth();
  noStroke();

  //float medArea = (width*height)/nbrParticles;
  midR = sqrt(((width*height)/dotamount)/PI);
  minR = midR;
  maxR = midR*midR*1;
  background(255);
}

void magic()
{
  if (frameCount % dancerslowness == 0) {
    int frameCtr = (frameCount/dancerslowness % picamount);
    currentpic = dancing[frameCtr];
  }

  for (int i = 0; i < dotamount;i++) {
    int px = (int)(dots[i].x/sc);
    int py = (int)(dots[i].y/sc);
    if (px >= 0 && px < currentpic.width && py >= 0 && py < currentpic.height) {
      int v = (int) red(currentpic.pixels[ py*currentpic.width + px ]);
      dots[i].rad = map(v/255.0, 0, 1, minR, maxR);
    }
  }


  for (int i = 0; i < dotamount; ++i) {
    Dot p = dots[i];
    p.fx = p.fy = p.wt = 0;
    p.vx*=dampforce;
    p.vy*=dampforce;
  }

  for (int i = 0; i <dotamount-1; ++i) {
    Dot p = dots[i];
    for (int j = i+1; j< dotamount; ++j) {
      Dot pj = dots[j];
      if (i== j||Math.abs(pj.x - p.x)> p.rad*minDistFactor || Math.abs(pj.y - p.y) > p.rad*minDistFactor)
        continue;

      double dx = p.x-pj.x;
      double dy = p.y-pj.y;
      double distance=Math.sqrt(dx*dx+dy*dy);

      double maxDist = (p.rad + pj.rad);
      double diff = maxDist - distance;
      if (diff > 0) {
        double scle = diff/maxDist;
        scle = scle*scle;
        p.wt += scle;
        pj.wt += scle;
        scle = scle*kSpeed/distance;
        p.fx += dx*scle;
        p.fy += dy*scle;
        pj.fx -= dx*scle;
        pj.fy -= dy*scle;
      }
    }
  }

  for (int i = 0; i < dotamount; ++i) {
    Dot p = dots[i];

    // keep within edges
    double dx, dy, distance, scle, diff;
    double maxDist = p.rad;
    // left edge  
    distance = dx = p.x - 0;    
    dy = 0;
    diff = maxDist - distance;
    if (diff > 0) {
      scle = diff/maxDist;
      scle = scle*scle;
      p.wt += scle;
      scle = scle*kSpeed/distance;
      p.fx += dx*scle;
      p.fy += dy*scle;
    }
    // right edge  
    dx = p.x - width;    
    dy = 0;
    distance = -dx;
    diff = maxDist-distance;
    if (diff > 0) {
      scle = diff/maxDist;
      scle = scle*scle;
      p.wt += scle;
      scle = scle*kSpeed/distance;
      p.fx += dx*scle;
      p.fy += dy*scle;
    }
    // top edge
    distance = dy = p.y - 0;    
    dx = 0;
    diff = maxDist-distance;
    if (diff > 0) {
      scle = diff/maxDist;
      scle = scle*scle;
      p.wt += scle;
      scle = scle*kSpeed/distance;
      p.fx += dx*scle;
      p.fy += dy*scle;
    }
    // bot edge  
    dy = p.y - height;    
    dx = 0;
    distance = -dy;
    diff = maxDist-distance;
    if (diff > 0) {
      scle = diff/maxDist;
      scle = scle*scle;
      p.wt += scle;
      scle = scle*kSpeed/distance;
      p.fx += dx*scle;
      p.fy += dy*scle;
    }
    if (p.wt > 0) {
      p.vx += p.fx/p.wt;
      p.vy += p.fy/p.wt;
    }
    p.x += p.vx;
    p.y += p.vy;
  }
}

void draw()
{ 
  magic();
  if(cam.available()) {
    cam.read();
  }
  opencv.loadImage(cam);
  opencv.useColor(RGB);
  output = opencv.getSnapshot();
  
  dimension = cam.width * cam.height;
  
  output.loadPixels();
  bright = 0;
  for (int i = 0;i<dimension; i+=10) { 
    color c = output.pixels[i];
    b = brightness(c);
    bright = bright +b;
  }
  total = bright/(dimension/10);  
  println(total);
  fill(0,99);
  rect(0,0,width,height);
  stroke(255,170-total);
  strokeWeight(2);
  for (int i=0; i<dotamount;i++) {
    point(dots[i].x,dots[i].y);
  }
}