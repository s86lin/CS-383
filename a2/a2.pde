import processing.video.*;
import gab.opencv.*;

OpenCV opencv;
Capture cam;
float b, bright;
float total = 255;
float scale = 0.5;
int dimension, count;
PImage output;

float NOISE_SCALE = 0.003;
int STEP = 20;
int m = 100;
Stars[] myStar= new Stars[m];

float x[] = {};
float y[] = {};
float dirx[] = {};
float diry[] = {};

void setup () {
  //size(800, 800);//Size of Display
  fullScreen();
  background(0);
  for (int j=0; j<myStar.length; j++) {
    //Initialises the stars
    myStar[j]= new Stars();
  }
  strokeWeight(2);
  cam = new Capture(this, int(800 * scale), int(800 * scale));

  opencv = new OpenCV(this, cam.width, cam.height);
  cam.start();

  // init to empty image
  output = new PImage(cam.width, cam.height);

  noStroke();
  smooth();
}


void drawPerlinCurve (int x, int y, float phase, int step, int count, color myColour) {
  pushMatrix();
  noFill();
  count = round(width*1.5/STEP);
  beginShape();
  stroke(myColour);
  for (int i=0; i< count; i++) {
    curveVertex(x, y);
    float angle = 2*PI*noise(x* NOISE_SCALE, y* NOISE_SCALE, phase* NOISE_SCALE);
    x += cos(angle)*step;
    y += sin(angle)*step;
  }
  endShape();
  popMatrix();
}


void draw() {
  if (cam.available() == true) {
    cam.read();
  }
  opencv.loadImage(cam);
  opencv.useColor(RGB);
  output = opencv.getSnapshot();
  dimension = cam.width * cam.height;
  output.loadPixels();
  bright = 0;
  for (int i = 0; i < dimension; i += 20) { 
    color c = output.pixels[i];
    b = brightness(c);
    bright = bright + b;
  }
  total = (bright/(dimension/20))+25; 
  if (total > 100) {
    total+=50;
  }
  total = 255-total;

  pushMatrix();
  popMatrix();
  int phase = frameCount / 2;
  for (int y = 0; y < height; y+=400) {
    if (total < 60) {
      color myColour = lerpColor(color(0, random(255), 255, 0), color(random(75, 255), random(255), random(130, 255), 0), y / height);
      drawPerlinCurve(width+50, y, phase, STEP, count, myColour);
    } else if (total < 75) {

      color myColour = lerpColor(color(0, random(100, 255), random(130, 255), total/4), color(random(128, 255), random(0, 100), random(130, 255), total/4), y / height);
      drawPerlinCurve(width+50, y, phase, STEP, count, myColour);
    } else {
      color myColour = lerpColor(color(0, random(255), 255, total/2), color(random(75, 255), random(255), random(130, 255), total/2), y / height);
      drawPerlinCurve(width+50, y, phase, STEP, count, myColour);
    }
  }
  
  for (int j=0; j<myStar.length; j++) {
    myStar[j].display();
    if (total < 50) {
      fill(random(10, total), total);
    } else {
      fill(random(10, total));
    }
  }    
  filter(BLUR, 1);

  if (total > 100) {
    if (frameCount % 50 == 0) {
      x = append(x, random(width));
      y = append(y, random(height));
      dirx = append(dirx, random(-1, 1));
      diry = append(diry, random(-1, 1));
    }

    int i = 0;
    while (i< x.length) {
      fill(total);
      ellipse(x[i], y[i], 2.5, 2.5);
      x[i] += dirx[i];
      y[i] += diry[i];
      i +=1;
    }
  }
}  



class Stars {
  float x;//x position of stars
  float y;//y position of stars
  Stars() {
    //generates random position on screen
    x=random(20, width-20);
    y=random(20, height-20);
  }
  void display() {
    noStroke();
    ellipse(x, y, 3, 3);
  }
}