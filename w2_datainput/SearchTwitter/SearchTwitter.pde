/*
 * Example inspired by the earlier tutorial by blprnt
 * See http://twitter4j.org/javadoc/ for the in-depth
 * documentation about the many thing you can do with
 * the twitter4j library
 */
import gohai.simpletweet.*;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.MediaEntity;
import gab.opencv.*;
import java.awt.*;

OpenCV opencv;
PImage steph;
PImage saadiya;

SimpleTweet simpletweet;
ArrayList<Status> tweets;
PImage webImg;
float scale = 0.5;

Rectangle[] faces;
MediaEntity[] photo;

void setup() {
  size(640, 480);
  frameRate(0.5);
  simpletweet = new SimpleTweet(this);

  /*
   * Create a new Twitter app on https://apps.twitter.com/
   * then go to the tab "Keys and Access Tokens"
   * copy the consumer key and secret and fill the values in below
   * click the button to generate the access tokens for your account
   * copy and paste those values as well below
   */
  simpletweet.setOAuthConsumerKey("QbATXW5y3rjHydiCxgxox64o8");
  simpletweet.setOAuthConsumerSecret("Vm4AEoCM6LAP90DZWbB1wkPF5VYBecrISpTGjahOTuiva0ZRyO");
  simpletweet.setOAuthAccessToken("900530280657633280-pJJs7Zy9h8lTxbjowvrZqBadbooc5gL");
  simpletweet.setOAuthAccessTokenSecret("AXnLtNEnuMJjvQAn4RDhLkrOXUG34ODFUiee5ERbpETsY");

  tweets = search("#saadiyastephcollab");
  Status current = tweets.get(frameCount % tweets.size());
  photo = current.getMediaEntities();

  String l = photo[0].getMediaURL();
  // Load image from a web server
  webImg = loadImage(l, "png");
  opencv = new OpenCV(this, webImg);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
  faces = opencv.detect();
  steph = loadImage("data/steph.png");
  saadiya = loadImage("data/saadiya.png");


}

void draw() {



  image(opencv.getInput(), 0, 0);
  
  if (faces !=null){
    
       for (int i = 0; i < faces.length; i++) {

      // scale the tracked faces to canvas size
      float s = 1 ;
     
      int x = int(faces[i].x * s);
      int y = int(faces[i].y * s);
      int w = int(faces[i].width * s);
      int h = int(faces[i].height * s);
      
       if ((i % 2)==0){
       image(steph, x, y, w, h);
       }
       else {
         image(saadiya, x,y,w,h);
       }
       }
  }
}


ArrayList<Status> search(String keyword) {
  // request 100 results
  Query query = new Query(keyword);
  query.setCount(100);

  try {
    QueryResult result = simpletweet.twitter.search(query);
    ArrayList<Status> tweets = (ArrayList)result.getTweets();
    // return an ArrayList of Status objects
 	return tweets;
  } catch (TwitterException e) {
    println(e.getMessage());
    return new ArrayList<Status>();
  }
}

void mousePressed() {
  String tweet = simpletweet.tweetImage(get(), "Made with Processing; Check out @_sdesai for input");
  println("Posted " + tweet);
}