import processing.video.*;
import emotionprocessing.*;
import ddf.minim.*;
import java.util.*;
import java.awt.*; 

Capture cam;
EmotionProcessing emo_recog;

Boolean pic = false;
FloatDict emotions;
FloatDict faceRect;
String emotion;
boolean playing = false; 
color curr;

Minim minim;
AudioPlayer song;


String happiness[] = {"happiness.mp3","happiness2.mp3","happiness.mp3"};
String sadness[] = {"sadness.mp3", "sadness2.mp3"};
String surprise[] = {"surprise.mp3", "surprise2.mp3"};
String neutral[] = {"neutral.mp3"};
String fear[] = {"fear.mp3"};
String disgust[] = {"disgust.mp3"};
String anger[] = {"anger.mp3"};
String contempt[] = {"contempt.mp3"};
public Map<String, String[]> Music = new HashMap<String, String[]>() {
  {
    put("happiness", happiness);
    put("sadness", sadness);
    put("surprise", surprise);
    put("neutral", neutral);
    put("fear", fear);
    put("disgust", disgust);
    put("anger", anger);
    put("contempt", contempt);
  }
};

public Map<String, Integer> EmotionColor = new HashMap<String, Integer>() {
  {
    put("happiness", #fff400);
    put("sadness", #0000e7);
    put("surprise", #00f9ff);
    put("neutral", #ffffff);
    put("fear", #800080);
    put("disgust", #008000);
    put("anger", #ec0909);
    put("contempt", #ffa500);
  }
};

void setup() {

  size(1440, 900);
  String[] cameras = Capture.list();
  cam = new Capture(this, 1440, 900);

  cam.start();    
  emo_recog= new EmotionProcessing("538b1f369acc408dac9dd16da8f3bf35");
}

void draw() {
  if (cam.available() == true) {
    cam.read();
  }
  image(cam, 0, 0); //image from camera
  stroke(255);


  if (song !=null) {

    strokeWeight(2);
    if (emotion !=null){
    stroke(EmotionColor.get(emotion));
    curr = EmotionColor.get(emotion);
    } else{
      stroke(curr);
    }
   for (int i = 0; i < song.bufferSize() - 5; i++)
    {
      float x1  =  map( i, 0, song.bufferSize(), 0, width );
      float x2  =  map( i+1, 0, song.bufferSize(), 0, width );
      line(x1, 50 + song.left.get(i)*105,x2, 50 + song.left.get(i+1)*105);
      line(x1, 90 + song.right.get(i)*105, x2, 90 + song.right.get(i+1)*105);
      line(x1, 130 + song.left.get(i)*105, x2, 130 + song.left.get(i+1)*105);
    }
    }
}

void mousePressed ()
{
  emotions= emo_recog.recognizeFromCamera(cam);
  faceRect = emo_recog.getFaceRectangle();
  emotions.sortValuesReverse();
  emotion = emotions.key(0);
  if (emotion != null) {
    if (playing == true) {
      minim.stop();
      playing = false;
    }
    minim = new Minim(this);
    int length = Music.get(emotion).length;
    int rand = new Random().nextInt(length); 
    println(rand);
    song = minim.loadFile(Music.get(emotion)[rand]);
    song.play();
    playing = true;
  }
}