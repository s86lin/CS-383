float opacity = 20;

class Agent {

  // current position
  float x, y;
  // previous position
  float px, py;
  float shade;
  // create agent that picks starting position itself


Agent() {
    // random starting position
    int m = 100; // margin
    x = random(m, width - m);
    y = random(m, height - m);
shade = 255 * int(random(0, 2));
  }
  

  // create agent at specific starting position
  Agent(float _x, float _y) {
    x = _x;
    y = _y;
  }

  void update() {
    // save last position
    px = x;
    py = y;
        // pick a new position
    x = x + random(-param, param);
    y = y + random(-param, param);
  }
    void draw() {
    // draw a line between last position
    // and current position
strokeWeight(1);
stroke(shade, 50);
    ellipse(px, py, 40, 40);

  }
}